import React, { Component } from "react";
import "./css/Memo.css";

export class Memo extends Component {
  //calling constructor
  constructor(props) {
    super(props);
    this.state = {
      round: 1,
      playerTurn: false,
    };
    this.clickedCount = 0;
    // this.userClickedDivs = new Array(0)
    this.autoClickedDivs = new Array(0);
    this.div_click = this.div_click.bind(this);
    this.refresh = this.refresh.bind(this);
  }

  componentDidMount() {
    console.log("in did mont calling auto click");
    this.click_randomly_on_divs();
  }
  componentDidUpdate() {
    if (!this.state.playerTurn) {
      this.click_randomly_on_divs();
    }
  }

  // randomly click on divs
  click_randomly_on_divs() {
    let timerid = setInterval(
      () => {
        const id = Math.floor(Math.random() * 4 + 1);
        const element = document.getElementById(`i-${id}`);
        element.style.backgroundColor = "grey";
        setTimeout(() => (element.style.backgroundColor = ""), 300);
        this.autoClickedDivs.push(element);
      },
      500,
      this.round
    );
    setTimeout(() => {
      clearInterval(timerid);
    }, this.state.round * 500);
  }

  // //for wrong click blinking
  blink() {
    let blink_element = document.querySelector(".divs-container");
    blink_element.style.backgroundColor = "red";
    setTimeout(
      () => (blink_element.style.backgroundColor = "rgb(255,255,255)"),
      500
    );
  }

  // // for wrong click
  wrong_click() {
    let timerid2 = setInterval(this.blink, 1000);
    setTimeout(() => {
      clearInterval(timerid2);
      alert("sorry wrong click!! Click on refresh to play again");
    }, 6000);
  }

  //After user click on div this function will get called.
  div_click = (div_id) => {
    this.clickedCount++;
    if (this.clickedCount <= this.state.round) {
      if (div_id !== this.autoClickedDivs[this.clickedCount - 1].id) {
        this.wrong_click();
      }
      if (this.clickedCount === this.state.round) {
        this.clickedCount = 0;
        this.autoClickedDivs = new Array(0);
        this.setState({ round: this.state.round + 1 });
      }
    }
  };

  //After user clicks on refresh this function will get called.
  refresh() {
    window.location.reload();
  }

  //rendering.
  render() {
    return (
      <div className="memo-container">
        <div className="divs-container">
          <div className="row">
            <div id="i-1" onClick={() => this.div_click("i-1")}></div>
            <div id="i-2" onClick={() => this.div_click("i-2")}></div>
          </div>
          <div className="row">
            <div id="i-3" onClick={() => this.div_click("i-3")}></div>
            <div id="i-4" onClick={() => this.div_click("i-4")}></div>
          </div>
        </div>
        <button type="button">start</button>
        <button type="button" onClick={this.refresh}>
          refresh
        </button>
      </div>
    );
  }
}

export default Memo;
